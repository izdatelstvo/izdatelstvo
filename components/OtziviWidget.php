<?php

namespace app\components;

use yii\base\Widget;
use app\models\Otzivi;

class OtziviWidget extends Widget{
    
  
    public function init(){
        parent::init();
    }
    
    public function run(){
        $post = Otzivi::find()->where(['activ' => 'yes'])->orderBy('id DESC')->all();
        
         return $this->render('otzivi', compact('post'));
       
        
    }
   

   
   
   
   
}