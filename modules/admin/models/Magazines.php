<?php

namespace app\modules\admin\models;

use yii\db\ActiveRecord;


class Magazines extends ActiveRecord{
    

    public static function tableName(){
        return 'magazines';
    }
    
    public $image;

   
   public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }
    
    public function rules()
    {
        return [];
    }
    
    
    public function attributeLabels()
    {
        return [
            "title" => "Наименование",
            "bookType" => "Тип книги",
            "dateUni" => "Дата публикации",
            "publ" => "Публикация",
            "publid" => "ID Публикации",
            "placePubl" => "Место публикации",
            "author_id" => "ID автора",
            "pages" => "Страницы",
            "langPubl" => "Язык публикации",
            "titleid" => "ID наименования",
            "issn" => "ISSN",
            "eissn" => "EISSN",
            "volume" => "Часть",
            "number" => "Номер",
            "altNumber" => "Альтернативный номер",
            "part" => "Часть",
            "issTitle" => "IssTitle",
        ];
    }
    
public function upload(){
        
        if ($this->validate()) { 
            $path = 'images/store/' . $this->image->baseName . '.' . $this->image->extension;
                $this->image->saveAs($path);
                $this->attachImage($path, true);
                @unlink($path);
                return true;
        } else {
            return false;
        }
        
    }
    
}