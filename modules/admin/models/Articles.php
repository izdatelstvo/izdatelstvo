<?php

namespace app\modules\admin\models;

use yii\db\ActiveRecord;


class Articles extends ActiveRecord{
    

    public static function tableName(){
        return 'articles';
    }
    
    public $image;

   
   public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }
    
    public function rules()
    {
        return [
        ];
    }
    
    
    public function attributeLabels()
    {
        return [
            'secTitle' => 'Секция',
            'pages' => 'Страницы',
            'artType' => 'Тип артикула',
            'authors_id' => 'ID авторов',
            'artTitle' => 'Название статьи',
            'abstract' => 'Аннотация',
            'text' => 'Текст',
        ];
    }
    
    public function upload(){
        
        if ($this->validate()) { 
            $path = 'images/store/' . $this->image->baseName . '.' . $this->image->extension;
                $this->image->saveAs($path);
                $this->attachImage($path, true);
                @unlink($path);
                return true;
        } else {
            return false;
        }
        
    }
    
}