<?php
namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\db\Query;
use app\modules\admin\models\Articles;
use app\modules\admin\models\Magazines;
use app\modules\admin\models\Authors;
use app\modules\admin\models\Keywords;
use app\modules\admin\models\KeywordsValues;
use app\modules\admin\models\References;
use app\modules\admin\models\Codes;
use app\modules\admin\models\operCards;
use app\modules\admin\models\ErrorLog;

class XmlParser extends Model
{
    /**
     * @var UploadedFile
     */
    public $xmlFile;
    static $fields;

    public function rules()
    {
        return [
            [['xmlFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xml'],
        ];
    }
    
    public function start($name)
    {
        $xmlFile = $name;
        
        $file = $_SERVER['DOCUMENT_ROOT']."/web/server/php/files/".$xmlFile;
        $xml = simplexml_load_file($file);

        self::prepare();
        self::parsMagazine($xml);
    }

    public function prepare(){
        $array = array(
            "operCards",
            "magazines",
            "articles",
            "authors"
        );
        foreach($array as $table){
            self::getFields($table);
        }
    }

	/* Получаем список всех полей нужных нам таблиц. Нужно для проверки существует данное поле в Mysql'е или нет */
    public function getFields($table){
        $query = (new Query())->select('*')->from('INFORMATION_SCHEMA.COLUMNS')->where(['TABLE_NAME'=>$table]);
        $rows = $query->all();

        $columns = array();
        foreach($rows as $key=>$value){
            $columns[] = $value['COLUMN_NAME'];
        }
        self::$fields[$table] = $columns;
    }

	/* Парсинг основного блока журнала */
    private function parsMagazine($xml){

        $titleid = $xml->titleid;

        if (Magazines::find()->where(['titleid' => $titleid])->exists()){    
            $magazine = Magazines::find()->where(['titleid' => $titleid])->one();
            echo "<p class='info'>Журнал уже существует, ID: ".$magazine->id."</p>";
        } else {
            $magazine = new Magazines;
            $magazine->save();
            echo "<p class='info'>Добавлен новый журнал, ID: ".$magazine->id."</p>";
        }

        $values = array();
        foreach($xml as $field => $value){
            $val = (string)$value;
        
            switch ($field){
                case "journalInfo":
                    $magazine->title = (string)$value->title;
                    break;
                case "operCard":
                    self::parsOperCards($value, $magazine->id);
                    break;
                case "issue":
                    foreach($value as $k => $issue){
                        switch ($k){
                            case "articles":
                                self::parsArticles($issue, $magazine->id);
                                break;
                            default:
                                if (in_array($k, self::$fields['magazines'])){
                                   $magazine->$k = $issue;
                                } else {
                                    self::addToLog("magazines", $k, $issue);
                                }
                                break;
                        }
                    }
                    break;
                default:
                    if (in_array($field, self::$fields['magazines'])){
                       $magazine->$field = $val;
                    } else {
                        self::addToLog("magazines", $field, $val);
                    }
                    break;
            }
        }
        $magazine->save();

        echo '<p class="success">XML-Файл успешно загружен! Дата загрузки: '.date("d.m.Y h:i:s", time()).'</p>';

        return true;
    }

	/* Парсинг блока Articles */
    private function parsArticles($xml, $magazine_id){
        $section = "";
        foreach($xml as $field=>$value){
            switch ($field){
                case "section":
                    $section = $value->secTitle;
                    break;
                case "article":
                    $article_caption = (string)$value->artTitles->artTitle;
                    if (Articles::find()->where(['artTitle' => $article_caption])->exists()){
                        $article = Articles::find()->where(['artTitle' => $article_caption])->one();
                        echo "<p class='info'>Статья уже существует, ID: ".$article->id."</p>";
                    } else {
                        $article = new Articles;   
                        $article->save(); 
                        echo "<p class='info'>Добавлена новая статья, ID: ".$article->id."</p>";
                    }
                    
                    $article->secTitle = $section;
                    $article->magazine_id = $magazine_id;
                    $authors = array();
                    foreach($value as $a_field => $a_value){
                        switch ($a_field){
                            case "authors":
                                foreach($a_value->author as $author){
                                    $authors[] = self::parsAuthor($author);
                                }
                                break;
                            case "abstracts":
                                $article->abstract = $a_value->abstract;
                                break;
                            case "artTitles":
                                $article->artTitle = (string)$a_value->artTitle;
                                break;
                            case "dates":
                                $article->dateReceived = date("Y-m-d", strtotime((string)$a_value->dateReceived));
                                break;
                            case "keywords":
                                self::setKeywords($a_value, $article->id);
                                break;
                            case "references":
                                self::setReferences($a_value, $article->id);
                                break;
                            case "codes":
                                self::setCodes($a_value, $article->id);
                                break;
                            default:
                                if (in_array($a_field, self::$fields['articles'])){
                                    $article->$a_field = $a_value;
                                } else {
                                    self::addToLog("articles", $a_field, $a_value);
                                }
                                break;
                        }
                    }
                    $article->authors_id = implode(",", $authors);
                    $article->save();
                    break;
            }

        }
        return true;
    }

    /* Парсинг блока Authors */
    private function parsAuthor($author_xml){
        $id = (int)$author_xml['id'];
        
        if ($id){
            if (Authors::find()->where(['inner_id' => $id])->exists()){ 
                $author = Authors::find()->where(['inner_id' => $id])->one();
                echo "<p class='info'>Автор уже существует, ID: ".$author->id."</p>";
            }
        }

        if (!isset($author)){
            $author = new Authors;   
            $author->save(); 
            echo "<p class='info'>Добавлен новый автор, ID: ".$author->id."</p>";
        }
        $author->inner_id = $id;

        $values = array();
        foreach($author_xml->individInfo as $array){
            foreach($array as $field => $value){
                if (in_array($field, self::$fields['authors'])){
                    $author->$field = $value;
                } else {
                    self::addToLog("authors", $field, $value);
                }
            }
            break;
        }
        $author->save();

        return $author->id;
    }

    /* Парсинг блока Keywords */
    private function setKeywords($xml, $article_id){
        foreach($xml->kwdGroup->keyword as $caption){
            $caption = (string)$caption;
            if (KeywordsValues::find()->where(['caption' => $caption])->exists()){    
                $keywordsValues = KeywordsValues::find()->where(['caption' => $caption])->one();
            } else {
                $keywordsValues = new KeywordsValues;
                $keywordsValues->caption = $caption;
                $keywordsValues->save();
            }

            if (!Keywords::find()->where(['article_id' => $article_id, "value_id" => $keywordsValues->id])->exists()){
                $keyword = new Keywords;
                $keyword->article_id = $article_id;
                $keyword->value_id = $keywordsValues->id;
                $keyword->save();
            }
        }
    }

    /* Следующие 2 функции можно потом объединить */
    private function setReferences($xml, $article_id){
        foreach($xml->reference as $caption){
            if (!References::find()->where(['caption' => $caption, "article_id" => $article_id])->exists()){
                $references = new References;
                $references->caption = $caption;
                $references->article_id = $article_id;
                $references->save();
            }
        }
    }

    private function setCodes($xml, $article_id){
        foreach($xml as $key=>$value){
            if (!Codes::find()->where(['name' => $key, "article_id" => $article_id])->exists()){
                $codes = new Codes;
                $codes->name = $key;
                $codes->value = $value;
                $codes->article_id = $article_id;
                $codes->save();
            }
        }
    }

    /* Парсинг блока OperCard */
    private function parsOperCards($xml, $magazine_id){
			if (operCards::find()->where(['magazine_id' => $magazine_id])->exists()){ 
			    $operCards = operCards::find()->where(['magazine_id' => $magazine_id])->one();
			} else {
				$operCards = new operCards;   
			   $operCards->save(); 
			}
			$operCards->magazine_id = $magazine_id;

			foreach($xml as $field => $value){

			if (in_array($field, self::$fields['operCards'])){
			  $operCards->$field = $value;
			} else {
			  self::addToLog("operCards", $field, $value);
			}

			}
			$operCards->save();
    }

    /* Добавление в лог ошибки */
    private function addToLog($type, $field, $value){
        $log = new ErrorLog;

        $type_array = array(
            "magazines" => "журнала",
            "articles" => "статьи",
            "authors" => "автора"
        );

        $log->type = $type;
        $log->field = $field;
        $log->value = $value;

        $log->save();

        echo "<p class='error'>У ".$type_array[$type]." отсутствует поле ".$field."</p>";
        return true;

    }
}
?>