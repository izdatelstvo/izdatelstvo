<?php

namespace app\modules\admin\models;

use yii\db\ActiveRecord;


class Authors extends ActiveRecord{
    

    public static function tableName(){
        return 'authors';
    }
    
    public $image;

   
   public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }
    
    public function rules()
    {
        return [
        ];
    }
    
    
    public function attributeLabels()
    {
        return [
            'inner_id' => 'ID автора',
            'surname' => 'Фамилия',
            'initials' => 'Инициалы',
            'orgName' => 'Название организации',
        ];
    }
    
}