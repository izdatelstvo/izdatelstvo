<?php

namespace app\modules\admin\models;

use yii\db\ActiveRecord;


class ErrorLog extends ActiveRecord{
    

    public static function tableName(){
        return 'errors_log';
    }

   
   public function behaviors()
    {
        return [];
    }
    
    public function rules()
    {
        return [
        ];
    }
    
    
    public function attributeLabels()
    {
        return [
            'caption' => 'Название',
        ];
    }
    
}