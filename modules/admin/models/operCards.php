<?php

namespace app\modules\admin\models;

use yii\db\ActiveRecord;


class operCards extends ActiveRecord{
    

    public static function tableName(){
        return 'opercards';
    }

   
   public function behaviors()
    {
        return [];
    }
    
    public function rules()
    {
        return [
        ];
    }
    
    
    public function attributeLabels()
    {
        return [
            'caption' => 'Название',
        ];
    }
    
}