<?php

namespace app\modules\admin\models;

use yii\db\ActiveRecord;


class Image extends ActiveRecord{
    
    public static function tableName(){
        return 'image';
    }
}