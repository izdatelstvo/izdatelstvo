<?php

namespace app\modules\admin\controllers;
use app\modules\admin\models\Authors;
use yii\web\Controller;
use yii\web\UploadedFile;
use Yii;

/**
 * Default controller for the `admin` module
 */
class AuthorsController extends AppAdminController
{


    public function actionIndex()
    {
        $posts = Authors::find()->all();

        return $this->render('index', compact('posts'));
    }
    
    public function actionEdit()
    {
        $id = Yii::$app->request->get('id');
        $post = Authors::find()->where(['id' => $id])->one();
        
        $edit = Authors::findOne($id);
        if(!empty(Yii::$app->request->post())){

            $post = Yii::$app->request->post();
            foreach($post['Authors'] as $key => $value){
                $edit->$key = $value;
            }

            if($edit->save()){
                Yii::$app->session->setFlash('success','Редактирование прошло успешно.');
                return $this->refresh();
            }else{
                Yii::$app->session->setFlash('error','Ошибка редактирования!');
            }
         }

        return $this->render('edit', compact('edit','post'));
    }
    
    
    public function actionActiv()
    {
        
        $id = Yii::$app->request->get('id');
        $activ = Yii::$app->request->get('activ');
        
        $edit = Authors::findOne($id);
        $edit->activ = $activ;
        $edit->save();
        
    }
    
    
    
    public function actionNew()
    {
        $edit = new Authors;
        if( $edit->load(Yii::$app->request->post())){

            $post = Yii::$app->request->post();
            foreach($post['Authors'] as $key => $value){
                $edit->$key = $value;
            }
   
            if($edit->save()){
                
                Yii::$app->session->setFlash('success','Автор добавлен успешно.');
                return $this->refresh();
            }else{
                Yii::$app->session->setFlash('error','Ошибка добавления!');
            }
         }
        return $this->render('new', compact('edit'));
        
    }
    
    
    
    public function actionDelete()
    {
        $id = Yii::$app->request->get('id');
        $customer = Authors::findOne($id);
        
        return $this->render('delete');
    }
    
    
}
