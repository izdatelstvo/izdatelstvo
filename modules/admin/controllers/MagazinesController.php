<?php

namespace app\modules\admin\controllers;
use app\modules\admin\models\Magazines;
use yii\web\Controller;
use yii\web\UploadedFile;
use Yii;

/**
 * Default controller for the `admin` module
 */
class MagazinesController extends AppAdminController
{


    public function actionIndex()
    {
        $posts = Magazines::find()->all();

        return $this->render('index', compact('posts'));
    }
    
    public function actionEdit()
    {
        $id = Yii::$app->request->get('id');
        $post = Magazines::find()->where(['id' => $id])->one();
        
        $edit = Magazines::findOne($id);
        if(!empty(Yii::$app->request->post())){

            $post = Yii::$app->request->post();
            foreach($post['Magazines'] as $key => $value){
                $edit->$key = $value;
            }

            if($edit->save()){
                Yii::$app->session->setFlash('success','Редактирование прошло успешно.');
                return $this->refresh();
            }else{
                Yii::$app->session->setFlash('error','Ошибка редактирования!');
            }
         }

        return $this->render('edit', compact('edit','post'));
    }
    
    
        public function actionActiv()
    {
        
        $id = Yii::$app->request->get('id');
        $activ = Yii::$app->request->get('activ');
        
        $edit = Magazines::findOne($id);
        $edit->activ = $activ;
        $edit->save();
        
    }
    
    
    
    public function actionNew()
    {
        $edit = new Magazines;
        if( $edit->load(Yii::$app->request->post())){
   
            if($edit->save()){
                Yii::$app->session->setFlash('success','Пост добавлен успешно.');
                return $this->refresh();
            }else{
                Yii::$app->session->setFlash('error','Ошибка добавления!');
            }
         }
        return $this->render('new', compact('edit'));
        
    }
    
    
    
        public function actionDelete()
    {
        $id = Yii::$app->request->get('id');
        $customer = Magazines::findOne($id);
        
        return $this->render('delete');
    }
    
    
}
