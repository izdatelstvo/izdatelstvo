<?php

use mihaildev\ckeditor\CKEditor;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Статьи. Редактирование';
?>


<h4><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Редактирования статьи №<?=$post->id; ?></h4>
<br>



<?php if(Yii::$app->session->hasFlash('success')): ?>
        
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <?php echo Yii::$app->session->getFlash('success'); ?>
        </div>
        
   <?php endif; ?>
   
   <?php if(Yii::$app->session->hasFlash('error')): ?>
        
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <?php echo Yii::$app->session->getFlash('error'); ?>
        </div>
        
   <?php endif; ?>


<div class="col-xs-12 col-sm-6 col-md-6">
    


    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']])?>
    
    <?php 
        $attributes = $post->attributeLabels();
        foreach($post->attributes as $key => $value){
            switch($key){
                case "id":
                    break;
                default:
                    if (in_array($key, $post->attributeLabels())) 
                        echo $form->field($edit, $key)->textInput(['value' => $value])->label($attributes[$key]);
                    else 
                        echo $form->field($edit, $key)->textInput(['value' => $value]);
                    break;
            }
           
        }
?>
</div>
<div class="col-xs-12">
<?php

        if (sizeof($other_attributs) > 0){
            echo '<div class="other_attributs">';
            if (isset($other_attributs['codes'])){
                echo '<div class="block"><p><strong>К статье прикреплены следующие коды:</strong></p>';
                foreach($other_attributs['codes'] as $codes){
                    echo "<p>".$codes->value."</p>";
                }
                echo '</div>';
            }
            if (isset($other_attributs['references'])){
                echo '<div class="block"><p><strong>References:</strong></p>';
                foreach($other_attributs['references'] as $references){
                    echo "<p>".$references->caption."</p>";
                }
                echo '</div>';
            }
            if (isset($other_attributs['keywords'])){
                echo '<div class="block"><p><strong>Ключевые слова:</strong></p>';
                foreach($other_attributs['keywords'] as $keywords){
                    echo "<p>".$keywords->caption."</p>";
                }
                echo '</div>';
            }
            echo '</div>';
        }
    ?>


    <?= html::submitButton('Сохранить', ['class' => 'btn btn-success'])?>
    <?php ActiveForm::end()?>
    
    <div class="clearfix"></div>

    
    
</div>





