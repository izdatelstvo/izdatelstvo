
/*jslint unparam: true */
/*global window, $ */
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'blueimp.github.io' ?
                '//jquery-file-upload.appspot.com/' : 'server/php/';
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                $('<p/>').text("Залитый файл: " + file.name).appendTo('#files');
                $("#files").append('<a href="#start_pars" data-file="' + file.name +'" class="btn btn-primary">Начать</a>');
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

    $(document).on("click", 'a[href="#start_pars"]', function(){      
        var _o = $(this);
        $.ajax({
            type: "POST",
            url: "/admin/parser",
            data: {
                file: _o.data("file")
            },
            success: function(html){
                $("#errors").html(html);
            }
        }) 
    })
});